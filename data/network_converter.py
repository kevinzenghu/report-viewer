import json

json_data = open('network.json').read()
data = json.loads(json_data)

viz = {
    'schemes': [],
    'pointLabels': data['pointLabels'],
    'nodeDecorators': []
}
graph = {
    'nodes': [],
    'links': []
}

for node in data['nodes']:
    graph['nodes'].append(node['pointLabels'])
    viz['nodeDecorators'].append({'x': node['x'], 'y': node['y'], 'schemeVal': node['schemeVal'], 'schemeColor': node['schemeColor']})

for link in data['links']:
    graph['links'].append([link['source'], link['target']])

fout = open('network_formatted.json', 'w')
fout.write(json.dumps({'viz': viz, 'graph': graph}))