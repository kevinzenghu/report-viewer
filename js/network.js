function generateNetwork(data, group) {

    var groups = data.groups;
    var nodes = data.graph.nodes;
    var links = data.graph.links;
    var bounds = data.viz.bounds;
    var nodeDecorators = data.viz.nodeDecorators;

    var canvas2d;
    var ctx2d;
    var viewOffsetX, viewOffsetY, viewScale;
    var nodeScale = 1.4;

    var whichColorScheme = 1;
    var whichSizeScheme = 1;

    var currentGroup = group;

    // Init canvas
    initGraph();
    initCanvas(true);    
    selectGroup(currentGroup);
    drawScene();

    window.onresize = function(event) {
        initCanvas(true);  
        drawScene();
    };

    //////////////////////////////
    // Default Buttons
    //////////////////////////////
    $("#scheme-" + whichColorScheme).addClass('active');
    $("#breadcrumb #scheme span").text($("#scheme-" + whichColorScheme).text());

    //////////////////////////////
    // Mouse Event Handlers, Visual
    //////////////////////////////
    $('.group-option').on("mouseover click", function() {
        deselectAllGroups();
    });

    $('.scheme-option').on("mouseover", function() {
        $(".scheme-option").removeClass('active');
        $(this).addClass('active');
        whichColorScheme = $(this).data('value');
        $("#breadcrumb #scheme span").text($("#scheme-" + whichColorScheme).text());
        drawScene();
    });

    $('.scheme-option').on("click", function() {
        $(".scheme-option").removeClass('active');
        $(this).addClass('active');
        whichColorScheme = $(this).data('value');
        $("#breadcrumb #scheme span").text($("#scheme-" + whichColorScheme).text());
        drawScene();
    });

    function initGraph() {
        // Pre-process node and link data
        for (var key in nodeDecorators) {
            var n = nodeDecorators[key];
            n.selected = false;
        }
    }

    function initCanvas(recenter) {
        $("#content").empty();
        $("#content").html('<canvas id="canvas"></canvas>');
        canvas2d = document.getElementById("canvas");
        try {
            // Set up 2d context
            ctx2d = canvas2d.getContext("2d");

            canvas2d.width = $(document).width() - 5;
            canvas2d.height = $(document).height() - 5;

            ctx2d.fillStyle = "#1c1c1c";
            ctx2d.fillRect(0, 0, canvas2d.width, canvas2d.height);

            if (recenter) {
                // Init view offset and scale
                var hScale = canvas2d.height/Math.abs(bounds[1] - bounds[0]);
                var wScale = canvas2d.width/Math.abs(bounds[3] - bounds[2]);
                if (hScale < wScale) {
                    viewOffsetX = -bounds[2] + 0.5*(canvas2d.width - hScale*Math.abs(bounds[3] - bounds[2]))/hScale;
                    viewOffsetY = bounds[0];
                    viewScale = hScale;
                } else {
                    viewOffsetX = -bounds[2];
                    viewOffsetY = bounds[0] + 0.5*(canvas2d.height - wScale*Math.abs(bounds[1] - bounds[0]))/wScale;
                    viewScale = wScale;
                }
            }
        } catch (e) { throw(e); }
        if (!ctx2d) {
            alert("Could not get context from canvas.");
        }
    }

    $("#canvas").mousewheel(function(event, delta) {
        var prevViewScale = viewScale;
        var tempViewScale = viewScale + 0.1 * delta;
        viewScale = Math.max(tempViewScale, 0.01);
        viewOffsetX += event.clientX * (1.0/viewScale - 1.0/prevViewScale);
        viewOffsetY += event.clientY * (1.0/viewScale - 1.0/prevViewScale);
    });

    // Animation tick
    function tick() {
        requestAnimFrame(tick);
        drawScene();
    }

    // Function to select nodes of all groups
    function selectGroup(i) {
        if (i !== "all") {
            // Iterate over nodes in group
            for (var j=0; j < groups[i].nodes.length; j++) {
                nodeDecorators[groups[i].nodes[j]].selected = true;
            }
        }
    }

    function selectAllGroups() {
        // Iterate over groups        
        for (var i=0; i < groups.length; i++) {
            // Iterate over nodes in group
            for (var j=0; j < groups[i].nodes.length; j++) {
                nodeDecorators[groups[i].nodes[j]].selected = true;
            }
        }
    }

    function deselectAllGroups() {
        // Iterate over groups        
        for (var i=0; i < groups.length; i++) {
            // Iterate over nodes in group
            for (var j=0; j < groups[i].nodes.length; j++) {
                nodeDecorators[groups[i].nodes[j]].selected = false;
            }
        }
    }

    // Function to naively draw bounding ellipse
    function drawEllipses() {

        // Iterate over groups
        for (var k=0; k < groups.length; k++) {
            var nodes = groups[k].nodes;
            var maxDistance = 0;
            var nodeA = {};
            var nodeB = {};

            // Iterate over origin nodes
            for (var i=0; i < nodes.length; i++) {
                var xA = nodeDecorators[nodes[i]].x;
                var yA = nodeDecorators[nodes[i]].y;

                // Iterate over destination nodes
                for (var j=i+1; j < nodes.length; j++) {
                    var xB = nodeDecorators[nodes[j]].x;
                    var yB = nodeDecorators[nodes[j]].y;

                    // Euclidean distance
                    var d = Math.sqrt(Math.pow(xA - xB, 2) + Math.pow(yA - yB, 2));

                    if (d > maxDistance) {
                        nodeA.x = xA;
                        nodeA.y = yA;
                        nodeB.x = xB;
                        nodeB.y = yB;
                        maxDistance = d;
                    }
                }
            }
            nodeA.x = toScreenX(nodeA.x);
            nodeA.y = toScreenY(nodeA.y);
            nodeB.x = toScreenX(nodeB.x);
            nodeB.y = toScreenY(nodeB.y);

            // ctx2d.save();
            // ctx2d.font = "bold 14px sans-serif";
            // ctx2d.fillStyle = "red";
            // ctx2d.fillText("A" + k, nodeA.x, nodeA.y);
            // ctx2d.fillText("B" + k, nodeB.x, nodeB.y);

            // ctx2d.strokeStyle = "red";
            // ctx2d.lineWidth = 5;
            // ctx2d.beginPath();
            // ctx2d.moveTo(nodeA.x, nodeA.y);
            // ctx2d.lineTo(nodeB.x, nodeB.y);
            // ctx2d.stroke();

            // ctx2d.restore();
        }
    }

    // Main drawing function
    function drawScene() {
        ctx2d.fillStyle = "#1c1c1c";
        ctx2d.fillRect(0, 0, window.innerWidth - 4, window.innerHeight - 4);
        drawLinks();
        drawNodes();
        ctx2d.drawImage(canvas2d, 0, 0);
    }

    function toScreenX(sx) {
        return viewScale*(viewOffsetX + sx);
    }
    function toScreenY(sy) {
        return viewScale*(viewOffsetY + sy);
    }

    function drawNodes() {
        var nodeData = {};
        var strokeR;
        var strokeG;
        var strokeB;
        var radgrad;

        var numNodes = nodes.length;
        for (var i=0; i < numNodes; i++) {
            selected = false;
            selected = nodeDecorators[i].selected;
            nodeData.x = toScreenX(nodeDecorators[i].x);
            nodeData.y = toScreenY(nodeDecorators[i].y);

            if (selected) {
                nodeData.r = 1.0;
                nodeData.g = 1.0;
                nodeData.b = 1.0;
            } else {
                nodeData.r = nodeDecorators[i].schemeColor[whichColorScheme].r/255.0;
                nodeData.g = nodeDecorators[i].schemeColor[whichColorScheme].g/255.0;
                nodeData.b = nodeDecorators[i].schemeColor[whichColorScheme].b/255.0;
            }
            nodeData.s = nodeScale*viewScale*nodeDecorators[i].schemeColor[whichSizeScheme].s;

            strokeR = Math.round(255.0*nodeData.r);
            strokeG = Math.round(255.0*nodeData.g);
            strokeB = Math.round(255.0*nodeData.b);

            radgrad = ctx2d.createRadialGradient(nodeData.x, nodeData.y , 0, nodeData.x, nodeData.y, 1.1*nodeData.s);
            radgrad.addColorStop(0.00, "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 1.0)");
            radgrad.addColorStop(0.45, "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 0.4)");
            radgrad.addColorStop(0.48, "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 0.4)");
            radgrad.addColorStop(0.53, "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 0.75)");
            radgrad.addColorStop(0.62, "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 0.85)");
            radgrad.addColorStop(0.65, "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 0.22)");
            radgrad.addColorStop(1.00, "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 0.0)");
            ctx2d.fillStyle = radgrad;
            ctx2d.fillRect(nodeData.x - 1.1*nodeData.s, nodeData.y - 1.1*nodeData.s, 2*1.1*nodeData.s, 2*1.1*nodeData.s);
            ctx2d.fillStyle = "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 1.0)";
            ctx2d.beginPath();
            ctx2d.arc(nodeData.x, nodeData.y, 6.0*nodeScale*viewScale, 0, Math.PI*2, true);
            ctx2d.closePath();
            ctx2d.fill();
        }

        // Transparent circling
        ctx2d.fillStyle="rgba(255,255,255,0.2)";
        ctx2d.beginPath();
        // Draw transparent circles
        for (var i=0; i < numNodes; i++) {
            selected = false;
            selected = nodeDecorators[i].selected;
            nodeData.x = toScreenX(nodeDecorators[i].x);
            nodeData.y = toScreenY(nodeDecorators[i].y);
            if (selected) {
                ctx2d.arc(nodeData.x, nodeData.y, 150 * viewScale, 0, Math.PI*2, true);
            }
        }
        ctx2d.closePath();
        ctx2d.fill();

        // ctx2d.lineWidth=1;
        // ctx2d.strokeStyle="white";
        // // Draw white border
        // for (var i=0; i < numNodes; i++) {
        //     selected = false;
        //     selected = nodeDecorators[i].selected;
        //     nodeData.x = toScreenX(nodeDecorators[i].x);
        //     nodeData.y = toScreenY(nodeDecorators[i].y);
        //     if (selected) {
        //         ctx2d.beginPath();
        //         ctx2d.arc(nodeData.x, nodeData.y, 150 * viewScale, 0, Math.PI*2, true);
        //         ctx2d.stroke();
        //         ctx2d.closePath();
        //     }
        // }
        
    }
    function drawLinks() {
        var linkData = {};
        var linkSource;
        var linkTarget;
        var sourceSelected;
        var targetSelected;
        var strokeR;
        var strokeG;
        var strokeB;

        var numLinks = links.length;
        for (var i=0; i < numLinks; i++) {
            linkSource = links[i][0];
            linkTarget = links[i][1];
            sourceSelected = nodeDecorators[linkSource].selected;
            targetSelected = nodeDecorators[linkTarget].selected;

            linkData.sx = toScreenX(nodeDecorators[linkSource].x);
            linkData.sy = toScreenY(nodeDecorators[linkSource].y);

            if (sourceSelected) {
                linkData.sr = 1.0;
                linkData.sg = 1.0;
                linkData.sb = 1.0;
            } else {
                linkData.sr = nodeDecorators[linkSource].schemeColor[whichColorScheme].r/255.0;
                linkData.sg = nodeDecorators[linkSource].schemeColor[whichColorScheme].g/255.0;
                linkData.sb = nodeDecorators[linkSource].schemeColor[whichColorScheme].b/255.0;
            }

            linkData.tx = toScreenX(nodeDecorators[linkTarget].x);
            linkData.ty = toScreenY(nodeDecorators[linkTarget].y);
            if (targetSelected) {
                linkData.tr = 1.0;
                linkData.tg = 1.0;
                linkData.tb = 1.0;
            } else {
                linkData.tr = nodeDecorators[linkTarget].schemeColor[whichColorScheme].r/255.0;
                linkData.tg = nodeDecorators[linkTarget].schemeColor[whichColorScheme].g/255.0;
                linkData.tb = nodeDecorators[linkTarget].schemeColor[whichColorScheme].b/255.0;
            }

            strokeR = Math.round(255.0*(linkData.sr + linkData.tr)/2.0);
            strokeG = Math.round(255.0*(linkData.sg + linkData.tg)/2.0);
            strokeB = Math.round(255.0*(linkData.sb + linkData.tb)/2.0);
            ctx2d.strokeStyle = "rgba(" + strokeR + ", " + strokeG + ", " + strokeB + ", 178)";
            ctx2d.beginPath();
            ctx2d.moveTo(linkData.sx, linkData.sy);
            ctx2d.lineTo(linkData.tx, linkData.ty);
            ctx2d.stroke();
        }
    }
}