function generateElements() {
    var menu_open = false;
    var current_menu = "none";
    var current_group = "all";
    var current_viz = "network";
    var current_explainer = 0;
    var groups_list = $('#groups-list');
    var schemes_list = $('#schemes-list');
    var explainers_list = $('#explainers-list');

    //////////////////////////////
    // Utility Functions
    //////////////////////////////

    // Animation
    window.requestAnimFrame = (function() {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function(/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
              window.setTimeout(callback, 1000/60);
            };
        })();

    // Check mobile browser
    window.mobilecheck = function() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check; };

    function closeAll() {
        $(".button, .menu, #breadcrumb, #title-container").transition({ x: '0' });
        $(".button").removeClass('active');
        menu_open = !menu_open;
        current_menu = "none";
    }

    //////////////////////////////
    // Page Configuration
    //////////////////////////////

    // Disable webpage scrolling
    $(document).bind('touchmove', function(e) {
        e.preventDefault();
    });

    //////////////////////////////
    // Element Creation
    //////////////////////////////

    // Populate group menu
    $('#groups-stats').text(analysisData.groups.length + " Groups");
    for (var i = 0; i < analysisData.groups.length; i++) {
        var group = analysisData.groups[i];
        groups_list.append('<div id="group-' + i + '" class="group-option option" data-value = ' + i + '>' + group.name + '</div>');
    }

    // Populate schemes menu
    $('#schemes-stats').text(analysisData.viz.schemes.length + " Schemes");
    for (var i = 0; i < analysisData.viz.schemes.length; i++) {
        var scheme = analysisData.viz.schemes[i];
        schemes_list.append('<div id="scheme-' + i + '" class="scheme-option option" data-value = ' + i + '>' + scheme + '</div>');
    }

    $(".down").mouseover(function() {
        var container = $(this).prev();
        setInterval(function() {
            $(container).animate({ scrollTop: $(container).scrollTop() + 10 + "px"}, 1);
        }, 40);
        // setInterval(, 100);
    });

    setListMaxHeight();
    setVizOptionHeight();

    function setListMaxHeight() {
        if (window.mobilecheck()) {
            $(".list").css("max-height", $(window).height() - $(".stats").height() - 50 + "px");
        } else {
            $(".list").css("max-height", $("#title").offset().top - $(".stats").height() - 50 + "px");
        }
    }

    function setVizOptionHeight() {
        var list_height = $("#title").offset().top - $("#viz-menu .stats").height();
        $(".viz-option").height(list_height * 0.1);
        $(".viz-option img").height(list_height * 0.1);
    }

    //////////////////////////////
    // Window Resize Events
    //////////////////////////////
    $(window).resize(function() {
        setListMaxHeight();
        setVizOptionHeight();
    });

    //////////////////////////////
    // Default Buttons
    //////////////////////////////
    $("#group-" + current_group).addClass('active');
    $("#network-button").addClass('active');

    //////////////////////////////
    // Mouse Event Handlers, Visual
    //////////////////////////////
    if (window.mobilecheck()) {
        $(".group-option").on('touchstart', function() {
            $(".group-option").removeClass('active');
            $(this).addClass('active');
        });

        $(".scheme-option").on('touchstart', function() {
            $(".scheme-option").removeClass('active');
            $(this).addClass('active');
        });

        $(".misc-option").on('touchstart', function() {
            $(".misc-option").removeClass('active');
            $(this).addClass('active');
        });

        $(".viz-option").on('touchstart', function() {
            $(".viz-option").removeClass('active');
            $(this).addClass('active');
        });

        $(".button").on('touchstart', function() {
            $(".button").removeClass('active');
            $(this).addClass('active');
        });

    } else {
        $(".group-option").mouseover(function() {
            $(".group-option").removeClass('active');
            $(this).addClass('active');
        });

        $(".scheme-option").mouseover(function() {
            $(".scheme-option").removeClass('active');
            $(this).addClass('active');
        });

        $(".explainer-option").mouseover(function() {
            $(".explainer-option").removeClass('active');
            $(this).addClass('active');
        });

        $(".misc-option").mouseover(function() {
            if (!$(this).hasClass('inactive')) {
                $(".misc-option").removeClass('active');
                $(this).addClass('active');
            }
        });

        $(".viz-option").mouseover(function() {
            if (!$(this).hasClass('inactive')) {
                $(".viz-option").removeClass('active');
                $(this).addClass('active');
            }
        });

        $(".button").mouseover(function() {
            $(".button").removeClass('active');
            $(this).addClass('active');
        });
    }

    $(".option").click(function() { closeAll(); });

    $('.group-option').on("mouseover click", function(e) {
        var group_val = $(this).data('value');
        current_group = group_val;
        if (group_val == 'all') {
            $("#group span").text('All Groups');
        } else {
            $("#group span").text(analysisData.groups[group_val].name);
        }

        if (current_viz === "network") {   
            $("#schemes-button").show();
            $("#explainers-button").hide();         
            generateNetwork(analysisData, current_group);
        } else if (current_viz === "histogram") {
            if (group_val === 'all') {
                $("#schemes-button").show();
                $("#explainers-button").hide();
                generateNetwork(analysisData, current_group);
            } else {
                generateHistogram(analysisData, current_group, current_explainer);
            }
        }
        if (e.type === "click") {
            if (group_val !== 'all') {
                current_group = group_val;
                $("#groups-menu").transition({ x: '0' });
                $("#viz-menu").transition({ x: '20em' });
                $("#schemes-button").show();
                $("#explainers-button").hide();
            }
        }
    });

    $("#group-all").click(function(e) {
        current_viz = 'network'
        current_group = 'all';
        $("#breadcrumb #viz span").text("Network");
        generateNetwork(analysisData, current_group);
    });

    // Hover colors
    if (window.mobilecheck()) {
        $(window).on('touchstart', function() {
            if (menu_open) {
                closeAll();
            }
        });

        $(".menu, .button, .option").on('touchstart', function(e) {
            return e.stopPropagation();
        });

    } else {
        $(".button").mouseout(function() {
            if (!menu_open) {
                $(this).stop();
                $(this).removeClass('active');
            }
        });

        // Close menu if click off menu or button
        $(window).click(function() {
            if (menu_open) {
                closeAll();
            }
        });
    }

    // Fallback on jQuery animation if no CSS animation support
    if (!Modernizr.csstransitions) {
        $.fn.transition = $.fn.animate;
    }

    $(".button").click(function() {
        var val = this.id.split('-')[0];
        if (!menu_open) {
            $("#" + val + "-menu").transition({ x: '20em' });
            $("#breadcrumb").transition({ x: '-30em' });
            current_menu = val;
            menu_open = !menu_open;

            if (window.mobilecheck()) {
                $("#title-container").transition({ x: '-30em' });
            }
        } else {
            if (current_menu !== val) {
                $("#" + current_menu + "-menu").transition({ x: '0' });
                $("#" + val + "-menu").transition({ x: '20em' });
                current_menu = val;
            } else {
                closeAll();
            }
        }
    });

    $(".back").click(function(e) {
        closeAll();
    });

    $(".menu, .button").click(function(e) {
        return e.stopPropagation();
    });

    //////////////////////////////
    // Mouse Event Handlers, Visualization
    //////////////////////////////
    $("#network-button").on("mouseover click", function(e) {
        current_viz = "network";
        $("#breadcrumb #viz span").text("Network");

        $("#schemes-button").show();
        $("#explainers-button").hide();

        $("#content").empty();
        $("#content").html('<canvas id="canvas"></canvas>');
        generateNetwork(analysisData, current_group);

        if (e.type === "click") {
            closeAll();
        }
    });

    $("#table-button").on("mouseover click", function() {
        // generateTable(analysisData);
    });
    $("#scatterplot-button").on("mouseover click", function() {
        // generateScatterplot(analysisData);
    });
    $("#histogram-button").on("mouseover click", function(e) {
        current_viz = "histogram";
        $("#breadcrumb #viz span").text("Histogram");
        $("#breadcrumb #scheme span").text("Explainer " + (current_explainer + 1));

        $("#content").empty();
        $("#content").html('<svg id="svg"></svg>');

        $("#schemes-button").hide();
        $("#explainers-button").show();

        // Populate explainers list
        explainers_list.empty();
        $('#explainers-stats').text(analysisData.plots.histograms[current_group].length + " Explainers");
        for (var i = 0; i < analysisData.plots.histograms[current_group].length; i++) {
            explainers_list.append('<div id="explainer-' + i + '" class="explainer-option option" data-value = ' + i + '>' + "Explainer " + (i + 1) + '</div>');
        }

        $('.explainer-option').on("mouseover click", function(e) {
            $(".explainer-option").removeClass('active');
            $(this).addClass('active');
            var explainer = $(this).data('value');
            current_explainer = explainer;
            $("#breadcrumb #scheme span").text("Explainer " + (explainer + 1));
            generateHistogram(analysisData, current_group, current_explainer);
            if (e.type === "click") {
                closeAll();
            }
        });

        generateHistogram(analysisData, current_group, current_explainer);
        if (e.type === "click") {
            closeAll();
        }

    });
    $("#box-button").click(function() {
        generateBoxplot(analysisData);
    });
}