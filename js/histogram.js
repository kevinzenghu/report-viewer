function generateHistogram(data, group, explainer) {
    if (typeof(group) === 'undefined') group = 1;
    if (typeof(explainer) === 'undefined') explainer = 0;
    histData = data.plots.histograms[group][explainer];

    window.onresize = function(event) {
        generateHistogram(data, group, explainer);
    };

    var margin = {
        top: $("#breadcrumb").offset().top + $("#breadcrumb").height() + 5,
        right: $("#button-container").width() + 5,
        bottom: $(window).height() - $("#title").offset().top + 50,
        left: 100
    },
    width = $(window).width() - margin.left - margin.right,
    height = $(window).height() - margin.top - margin.bottom;

    // Generate labels
    var labels = [];
    for (var i=0; i<histData.domain[1] - 1; i++) {
        var label = i + "-" + (i + 1);
        labels[i] = label;
    }

    var x0 = d3.scale.ordinal()
        .rangeRoundBands([0, width], 0.15);

    var x1 = d3.scale.ordinal();

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = d3.scale.ordinal()
        .range(["#bf2e1a", "#f7961d"]);

    var xAxis = d3.svg.axis()
        .scale(x0)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickFormat(d3.format(".2f"));

    $("#content").empty();
    var svg = d3.select("#content").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    x0.domain(labels);
    x1.domain([0, 1]).rangeRoundBands([0, x0.rangeBand()]);
    y.domain([0, 1]);

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .attr("text-anchor", "end")
        .attr("transform", function(d, i) {return "translate(" + -15 + "," + 15 + ")rotate(-90)"; });

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end");

    var zippedData = _.zip.apply(_, histData.counts);
    var processedData = [];
    for (var i=0; i<zippedData.length; i++) {
        var temp = {
            "bin": i,
            "values": []
        };
        for (var j=0; j<zippedData[i].length; j++) {
            temp["values"].push({
                "name": j,
                "value": zippedData[i][j]
            });
        }
        processedData.push(temp);
    }

    var bin = svg.selectAll(".bin")
        .data(processedData)
      .enter().append("g")
        .attr("class", "g")
        .attr("transform", function(d) { return "translate(" + x0(labels[d.bin]) + ", 0)"; });

    bin.selectAll("rect")
        .data(function(d) { return d.values; })
      .enter().append("rect")
        .attr("width", x1.rangeBand())
        .attr("x", function(d) { return x1(d.name); })
        .attr("y", function(d) { return y(d.value) - 1; })
        .attr("height", function(d) { return height - y(d.value); })
        .style("fill", function(d) { return color(d.name); });

    var legend = svg.selectAll(".legend")
        .data([data.groups[group].name, "All Other Groups"])
      .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

    legend.append("rect")
        .attr("x", width - 18)
        .attr("y", - 50)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

    legend.append("text")
        .attr("x", width - 24)
        .attr("y", -42)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function(d) { return d; });
}