// Generated by CoffeeScript 1.6.3
(function() {
  var generateBoxplot, root;

  generateBoxplot = function(data) {
    var chart, height, margin, max, min, outliers, quartiles, svg, width;
    data = data.plots.boxPlots;
    outliers = data.outliers;
    quartiles = data.quartiles;
    console.log(outliers, quartiles);
    margin = {
      top: 10,
      right: 50,
      bottom: 20,
      left: 50
    };
    width = 120 - margin.left - margin.right;
    height = 500 - margin.top - margin.bottom;
    min = Infinity;
    max = -Infinity;
    chart = d3.box().whiskers(quartiles[4] - quartiles[1]).width(width).height(height);
    return svg = d3.select("body").selectAll("svg").data(data).enter().append("svg").attr("class", "box").attr("width", width + margin.left + margin.right).attr("height", height + margin.bottom + margin.top).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")").call(chart);
  };

  root = window || this;

  root.generateBoxplot = generateBoxplot;

}).call(this);
