//////////////////////////////
// Initializations
//////////////////////////////

var analysisData = {
    "groups": [
    {'name': 'Young Firefly', 'nodes': [331, 144, 236, 210, 317, 188, 195, 157, 226, 194]},
    {'name': 'Bold Bird', 'nodes': [420, 460, 485, 483, 11]},
    {'name': 'Wandering Silence', 'nodes': [156, 165, 278, 370, 164]},
    {'name': 'Misty Glitter', 'nodes': [73, 431, 419, 352, 207]},
    {'name': 'Silencing Winter', 'nodes': [356, 470, 299, 307, 424]},
    {'name': 'Summer Water', 'nodes': [88, 57, 443, 493, 184, 179]},
    {'name': 'Hidden Forest', 'nodes': [212, 229, 202, 336, 353]}
    ],
    "plots": {
        "boxplots": [{
            columnId: 3,
            quartiles: [[0.1, 0.3, 0.4, 0.6, 0.8]],
            outliers: [[0.01, 0.02, 0.91]]
        }],
        "histograms": generateHistogramData()
    }
};

//////////////////////////////
// Utility Functions
//////////////////////////////

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

//////////////////////////////
// Main Function
//////////////////////////////

d3.json('data/network_formatted.json', function(data) {
    // Fix network data (flip y coordinates)
    for (var key in data.viz.nodeDecorators) {
        var n = data.viz.nodeDecorators[key];
        n.selected = false;
            n.y = -n.y;
        }

    analysisData.viz = data.viz;
    analysisData.graph = data.graph;

    generateHistogramData();
    generateElements();
    $('#content').append('<canvas id="canvas"></canvas>');
    generateNetwork(analysisData, "all");
});

//////////////////////////////
// Fake Data Generation
//////////////////////////////

function generateHistogramData() {
    var numExplainers = getRandomInt(3, 6);
    var upperLimit = getRandomInt(8, 16);
    var binCount = upperLimit;
    var comparisons = 2;
    var histogramData = [];
    var numGroups = 7;

    var peak1 = getRandomInt(1, 6);
    var peak2 = getRandomInt(peak1 + 2, upperLimit);

    // For every group
    for (var x=0; x < numGroups; x++) {
        var groupData = [];
        // Iterate over explains
        for (var i=0; i < numExplainers; i++) {
            var counts = [];
            // Iterate over comparisons (two, current group vs. all)
            for (var j=0; j < comparisons; j++) {
                var temp = [];
                var peak;
                if (j === 0) peak = peak2;
                else peak = peak1;
                // Iterate over bins
                for (var k=0; k < binCount - 1; k++) {
                    temp.push((getRandomInt(80, 100) / 100.0) * (Math.abs(peak - k) / upperLimit) );
                }
                counts.push(temp);
            }
            var explainData = {
                "binCount": binCount,
                "domain": [0, upperLimit],
                "counts": counts
            };
            groupData[i] = explainData;
        }
        histogramData[x] = groupData;
    }
    return histogramData;
}